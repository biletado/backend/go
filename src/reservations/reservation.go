package reservations

import (
	"biletado/src/cors"
	"biletado/src/pgtypes"
	"encoding/json"
	"fmt"
	"github.com/go-pg/pg/v10"
	"github.com/google/uuid"
	"github.com/gorilla/mux"
	"io/ioutil"
	"net/http"
)

type Reservation struct {
	Id     uuid.UUID    `pg:",pk,notnull,type:uuid,default:uuid_generate_v4()" json:"id"`
	From   pgtypes.Date `pg:",notnull,type:date" json:"from"`
	To     pgtypes.Date `pg:",notnull,type:date" json:"to"`
	RoomId uuid.UUID    `pg:",notnull,type:uuid" json:"room_id"`
}

func HandleReservation(db *pg.DB) http.Handler {

	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		muxVars := mux.Vars(r)
		id, _ := uuid.Parse(muxVars["id"])
		cors.AddHeaders(w, r)
		w.Header().Set("Content-Type", "application/json")
		switch r.Method {
		case "GET":
			handleReservationGet(w, r, db, id)
		case "PUT":
			handleReservationPut(w, r, db, id)
		case "DELETE":
			handleReservationDelete(w, r, db, id)
		default:
			w.WriteHeader(http.StatusMethodNotAllowed)
		}
	})
}

func handleReservationGet(w http.ResponseWriter, r *http.Request, db *pg.DB, id uuid.UUID) {
	reservation := &Reservation{Id: id}
	err := db.Model(reservation).WherePK().Select()
	if err != nil {
		panic(err)
	}
	b, err := json.Marshal(&reservation)
	fmt.Fprintf(w, string(b))
}

func handleReservationPut(w http.ResponseWriter, r *http.Request, db *pg.DB, id uuid.UUID) {
	var reservation Reservation
	body, err := ioutil.ReadAll(r.Body)
	if err != nil {
		panic(err)
	}
	json.Unmarshal(body, &reservation)
	reservation.Id = id
	model := db.Model(&reservation)
	exists, err := model.Where("id = ?", id.String()).Exists()
	if exists {
		_, err = model.Update(&reservation)
	} else {
		_, err = model.Insert(&reservation)
	}
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		panic(err)
	}
	w.WriteHeader(http.StatusNoContent)
}

func handleReservationDelete(w http.ResponseWriter, r *http.Request, db *pg.DB, id uuid.UUID) {
	var reservation Reservation
	model := db.Model(&reservation)
	exists, err := model.Where("id = ?", id.String()).Exists()
	if exists {
		_, err = model.Where("id = ?", id.String()).Delete()
		w.WriteHeader(http.StatusNoContent)
	} else {
		w.WriteHeader(http.StatusNotFound)
	}
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		panic(err)
	}
}

func HandleReservations(db *pg.DB) http.Handler {

	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		urlParams := r.URL.Query()
		roomId := uuid.Nil
		if len(urlParams["room_id"]) > 0 {
			roomId, _ = uuid.Parse(urlParams["room_id"][0])
		}
		cors.AddHeaders(w, r)
		w.Header().Set("Content-Type", "application/json")
		switch r.Method {
		case "GET":
			handleReservationsGet(w, r, db, roomId)
		case "POST":
			handleReservationsPost(w, r, db)
		default:
			w.WriteHeader(http.StatusMethodNotAllowed)
		}
	})
}

func handleReservationsGet(w http.ResponseWriter, r *http.Request, db *pg.DB, roomId uuid.UUID) {
	var reservations []Reservation
	var err error
	if roomId != uuid.Nil {
		err = db.Model(&reservations).Where("room_id = ?", roomId.String()).Select()
	} else {
		err = db.Model(&reservations).Select()
	}
	if err != nil {
		panic(err)
	}
	b, err := json.Marshal(&reservations)
	fmt.Fprintf(w, string(b))
}

func handleReservationsPost(w http.ResponseWriter, r *http.Request, db *pg.DB) {
	var reservation Reservation
	body, err := ioutil.ReadAll(r.Body)
	if err != nil {
		panic(err)
	}
	json.Unmarshal(body, &reservation)
	_, err = db.Model(&reservation).Insert(&reservation)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		panic(err)
	}
	b, err := json.Marshal(&reservation)
	w.WriteHeader(http.StatusCreated)
	fmt.Fprintf(w, string(b))
}
