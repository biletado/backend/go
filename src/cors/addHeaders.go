package cors

import (
	"net/http"
	"regexp"
)

func AddHeaders(w http.ResponseWriter, r *http.Request) {
	if r.Header["Origin"] != nil {
		if match, _ := regexp.MatchString("^http://localhost(:[0-9]+)?$", r.Header["Origin"][0]); match {
			w.Header().Set("Access-Control-Allow-Origin", r.Header["Origin"][0])
			w.Header().Set("Vary", "Origin")
		}
		w.Header().Set("Access-Control-Allow-Headers", "Authorization, Content-Type")
		w.Header().Set("Access-Control-Allow-Credentials", "true")
		w.Header().Set("Access-Control-Allow-Methods", "GET, OPTIONS, POST, PUT")
	}
}
