package assets

import (
	"biletado/src/cors"
	"encoding/json"
	"fmt"
	"github.com/go-pg/pg/v10"
	"github.com/google/uuid"
	"github.com/gorilla/mux"
	"net/http"
	"time"
)

type Building struct {
	Id          uuid.UUID  `pg:",pk,notnull,type:uuid,default:uuid_generate_v4()" json:"id"`
	Name        string     `pg:",notnull" json:"name"`
	CountryCode string     `json:"country_code"`
	Streetname  string     `json:"streetname"`
	Housenumber string     `json:"housenumber"`
	City        string     `json:"city"`
	Postalcode  string     `json:"postalcode"`
	DeletedAt   *time.Time `pg:",soft_delete" json:"deleted_at,omitempty"`
}

type BuildingCollection struct {
	Buildings *[]Building `json:"buildings"`
}

func HandleBuilding(db *pg.DB) http.Handler {

	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		muxVars := mux.Vars(r)
		cors.AddHeaders(w, r)
		w.Header().Set("Content-Type", "application/json")
		if r.Method == "GET" {
			id, _ := uuid.Parse(muxVars["id"])
			building := &Building{Id: id}
			err := db.Model(building).WherePK().Select()
			if err != nil {
				panic(err)
			}
			b, err := json.Marshal(&building)
			fmt.Fprintf(w, string(b))
		} else {
			w.WriteHeader(http.StatusMethodNotAllowed)
		}
	})
}

func HandleBuildings(db *pg.DB) http.Handler {

	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		cors.AddHeaders(w, r)
		w.Header().Set("Content-Type", "application/json")
		if r.Method == "OPTIONS" {
			return
		}
		if r.Method == "GET" {
			var buildings []Building
			urlParams := r.URL.Query()
			if len(urlParams["include_deleted"]) > 0 && urlParams["include_deleted"][0] == "true" {
				err := db.Model(&buildings).AllWithDeleted().Select()
				if err != nil {
					panic(err)
				}
			} else {
				err := db.Model(&buildings).Select()
				if err != nil {
					panic(err)
				}
			}
			buildingCollection := &BuildingCollection{Buildings: &buildings}
			b, err := json.Marshal(&buildingCollection)
			if err != nil {
				panic(err)
			}
			fmt.Fprintf(w, string(b))
		} else {
			w.WriteHeader(http.StatusMethodNotAllowed)
		}
	})
}
