package assets

import (
	"biletado/src/cors"
	"encoding/json"
	"fmt"
	"github.com/go-pg/pg/v10"
	"github.com/google/uuid"
	"github.com/gorilla/mux"
	"net/http"
	"time"
)

type Storey struct {
	Id         uuid.UUID  `pg:",pk,notnull,type:uuid,default:uuid_generate_v4()" json:"id"`
	Name       string     `pg:",notnull" json:"name"`
	BuildingId uuid.UUID  `pg:",notnull,type:uuid" json:"building_id"`
	Building   *Building  `pg:"rel:has-one" json:"building,omitempty"`
	DeletedAt  *time.Time `pg:",soft_delete" json:"deleted_at,omitempty"`
}

type StoreyCollection struct {
	Storeys *[]Storey `json:"storeys"`
}

func HandleStorey(db *pg.DB) http.Handler {

	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		muxVars := mux.Vars(r)
		cors.AddHeaders(w, r)
		w.Header().Set("Content-Type", "application/json")
		if r.Method == "GET" {
			id, _ := uuid.Parse(muxVars["id"])
			storey := &Storey{Id: id}
			err := db.Model(storey).WherePK().Select()
			if err != nil {
				panic(err)
			}
			b, err := json.Marshal(&storey)
			fmt.Fprintf(w, string(b))
		} else {
			w.WriteHeader(http.StatusMethodNotAllowed)
		}
	})
}

func HandleStoreys(db *pg.DB) http.Handler {

	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		urlParams := r.URL.Query()
		buildingId := uuid.Nil
		if len(urlParams["building_id"]) > 0 {
			buildingId, _ = uuid.Parse(urlParams["building_id"][0])
		}
		cors.AddHeaders(w, r)
		w.Header().Set("Content-Type", "application/json")
		switch r.Method {
		case "GET":
			handleStoreysGet(w, r, db, buildingId)
		default:
			w.WriteHeader(http.StatusMethodNotAllowed)
		}
	})
}

func handleStoreysGet(w http.ResponseWriter, r *http.Request, db *pg.DB, buildingId uuid.UUID) {
	var storeys []Storey
	var err error
	if buildingId != uuid.Nil {
		err = db.Model(&storeys).Where("building_id = ?", buildingId.String()).Select()
	} else {
		err = db.Model(&storeys).Select()
	}
	if err != nil {
		panic(err)
	}
	storeyCollection := &StoreyCollection{Storeys: &storeys}
	b, err := json.Marshal(&storeyCollection)
	fmt.Fprintf(w, string(b))
}
