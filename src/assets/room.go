package assets

import (
	"biletado/src/cors"
	"encoding/json"
	"fmt"
	"github.com/go-pg/pg/v10"
	"github.com/google/uuid"
	"github.com/gorilla/mux"
	"net/http"
	"time"
)

type Room struct {
	Id        uuid.UUID  `pg:",pk,notnull,type:uuid,default:uuid_generate_v4()" json:"id"`
	Name      string     `pg:",notnull" json:"name"`
	StoreyId  uuid.UUID  `pg:",notnull,type:uuid" json:"storey_id"`
	Storey    *Storey    `pg:"rel:has-one" json:"storey,omitempty"`
	DeletedAt *time.Time `pg:",soft_delete" json:"deleted_at,omitempty"`
}

type RoomCollection struct {
	Rooms *[]Room `json:"rooms"`
}

func HandleRoom(db *pg.DB) http.Handler {

	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		muxVars := mux.Vars(r)
		cors.AddHeaders(w, r)
		w.Header().Set("Content-Type", "application/json")
		if r.Method == "GET" {
			id, _ := uuid.Parse(muxVars["id"])
			room := &Room{Id: id}
			err := db.Model(room).WherePK().Select()
			if err != nil {
				panic(err)
			}
			b, err := json.Marshal(&room)
			fmt.Fprintf(w, string(b))
		} else {
			w.WriteHeader(http.StatusMethodNotAllowed)
		}
	})
}

func HandleRooms(db *pg.DB) http.Handler {

	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		cors.AddHeaders(w, r)
		w.Header().Set("Content-Type", "application/json")
		if r.Method == "GET" {
			var rooms []Room
			err := db.Model(&rooms).Select()
			if err != nil {
				panic(err)
			}
			roomCollection := &RoomCollection{Rooms: &rooms}
			b, err := json.Marshal(&roomCollection)
			fmt.Fprintf(w, string(b))
		} else {
			w.WriteHeader(http.StatusMethodNotAllowed)
		}
	})
}
